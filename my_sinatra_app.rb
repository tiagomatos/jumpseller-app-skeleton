class MySinatraApp < Sinatra::Base
  set :protection, :except => :frame_options

  get '/' do
    client = OAuth2::Client.new(client_id, client_secret, :site => oauth_host)
    redirect client.auth_code.authorize_url(:client_id => client_id, :redirect_uri => callback_uri, :scope => 'read_products')
  end

  get '/callback' do
    client = OAuth2::Client.new(client_id, client_secret, :site => oauth_host)
    token = client.auth_code.get_token(params[:code], redirect_uri: callback_uri)

    redirect "/products?token=#{token.token}"
  end


  get '/products' do
    headers =  {
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'Authorization' => "Bearer #{params[:token]}"
    }
    @products = HTTParty.get('https://api.jumpseller.com/v1/products.json', {headers: headers})
    erb :products
  end

  # find it on your Jumpseller Store's Admin Panel
  def client_id
    'CLIENT ID'
  end

  # find it on your Jumpseller Store's Admin Panel
  def client_secret
    'CLIENT SECRET'
  end

  def oauth_host
    'https://accounts.jumpseller.com'
  end

  def callback_uri
    ''
  end

end
